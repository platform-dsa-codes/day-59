/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        // If either list is empty, there can't be an intersection
        if (headA == null || headB == null) {
            return null;
        }
        
        ListNode pA = headA;
        ListNode pB = headB;
        
        // Iterate through both lists until either pA or pB reaches the end
        while (pA != pB) {
            // Move pA to the next node in listA
            pA = (pA == null) ? headB : pA.next;
            // Move pB to the next node in listB
            pB = (pB == null) ? headA : pB.next;
        }
        
        // If pA or pB is null, it means there's no intersection
        // Otherwise, pA (or pB) is the intersection point
        return pA;
    }
}
